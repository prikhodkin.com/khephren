<?php $__env->startSection('content'); ?>
  <?php echo $__env->make('blocks.hero.hero', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('blocks.about.about', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('blocks.activity.activity', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php echo $__env->make('blocks.portfolio.portfolio', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="container">
    <?php echo $__env->make('blocks.advantages.advantages', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('blocks.clients.clients', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->make('blocks.contact.contact', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>