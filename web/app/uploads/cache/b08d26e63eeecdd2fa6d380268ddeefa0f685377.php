<li class="advantages__item">
  <img src="<?php echo e($row['a-image']); ?>" alt="<?php echo e($row['a-text']); ?>" class="advantages__icon">
  <span class="advantages__accent"><?php echo e($row['a-accent']); ?></span>
  <p class="advantages__info"><?php echo e($row['a-text']); ?></p>
</li>
