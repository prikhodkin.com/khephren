<section class="clients">
  <h2 class="clients__title subtitle"><?php echo e(get_field('clients-title', 'option')); ?></h2>
  <ul class="clients__list">
    <?php echo App::printRepeatorField('clients-list', 'blocks.clients.clients-item', 'option'); ?>

  </ul>
</section>
