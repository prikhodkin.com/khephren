<li class="advantages__item">
  <img src="<?php echo e($row['icon']); ?>" alt="<?php echo e($row['text']); ?>" class="advantages__icon">
  <p class="advantages__info"><?php echo $row['text']; ?></p>
</li>
