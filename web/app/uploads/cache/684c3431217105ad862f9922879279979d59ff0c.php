<section class="contact">
  <div class="contact__body">
    <h2 class="contact__title title">Contact</h2>
    <ul class="contact__list">
      <li class="contact__item contact__item--phone"><a href="<?php echo e(App::NormalizePhone(get_field('phone', 'option'))); ?>"><?php echo e(get_field('phone', 'option')); ?></a></li>
      <li class="contact__item contact__item--email"><a href="<?php echo e(get_field('email', 'option')); ?>"><?php echo e(get_field('email', 'option')); ?></a></li>
      <li class="contact__item contact__item--address"><?php echo e(get_field('address','option')); ?></li>
      <li class="contact__item contact__item--work"><?php echo e(get_field('work','option')); ?></li>
    </ul>
    <a href="javascript://" class="contact__button button get-popup" data-popup="callback">Contacter</a>
    <ul class="contact__partners">
      <?php echo App::printRepeatorField('contact-partners', 'blocks.contact.contact-partner', 'option'); ?>

    </ul>
  </div>
  <div class="contact__map">
    <iframe src="https://maps.google.com/maps?q=15%20rue%20Joseph%20Marie%20Jacquard,%E2%80%A8Aulnay-sous-Bois,%2093600&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
  </div>
</section>
