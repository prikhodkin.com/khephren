<li class="portfolio__item swiper-slide">
  <div class="portfolio__info">
    <div class="portfolio__inner">
      <p class="portfolio__name"><?php echo e($row['name']); ?></p>
      <div class="portfolio__content">
        <?php echo $row['content']; ?>

      </div>
    </div>
  </div>
  <div class="swiper-container gallery">
    <ul class="gallery__list swiper-wrapper">
      <?php $__currentLoopData = $row['gallery']; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
      <li class="gallery__item swiper-slide">
        <img src="<?php echo e($item['image']); ?>">
      </li>
      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    </ul>
    <div class="gallery__controls controls">
      <button class="controls__button controls__button--prev" aria-label="prev">
        <?php echo $__env->make('partials.icons.prev', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </button>
      <div class="controls__counter">
        <span class="controls__current">1</span>/<span class="controls__length">6</span>
      </div>
      <button class="controls__button controls__button--next" aria-label="next">
        <?php echo $__env->make('partials.icons.next', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </button>
    </div>
  </div>
</li>
