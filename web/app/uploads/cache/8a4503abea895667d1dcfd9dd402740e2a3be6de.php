<div class="contact-bottom">
  <div class="contact-bottom__body">
    <p class="contact-bottom__title">Information complémentaire :</p>
    <div class="contact-bottom__text">
      <?php echo get_field('contact-bottom-text'); ?>

    </div>
  </div>
  <ul class="contact-bottom__list">
    <?php echo App::printRepeatorField('contact-bottom', 'blocks.contact.contact-bottom-item'); ?>

  </ul>
</div>
