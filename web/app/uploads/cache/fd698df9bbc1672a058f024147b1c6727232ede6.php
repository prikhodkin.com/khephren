<section class="about">
  <div class="about__body">
    <div class="about__inner">
      <h2 class="about__title title">Nous en quelques mots</h2>
      <div class="about__content">
        <?php echo get_field('about-content',get_option('page_on_front') ); ?>

        <a href="javascript://" class="about__button button get-popup" data-popup="callback">Savoir plus</a>
      </div>
    </div>
    <img src="<?php echo e(get_field('about-image', get_option('page_on_front'))); ?>" alt="Nous en quelques mots" class="about__img">
  </div>
</section>
