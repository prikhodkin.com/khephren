<section class="hero">
  <img src="<?php echo get_field('background', get_option('page_on_front')); ?>" alt="<?php echo e(get_field('hero-title', get_option('page_on_front'))); ?> " class="hero__img">

  <div class="hero__inner">
    <h2 class="hero__title subtitle"><?php echo e(get_field('hero-title', get_option('page_on_front'))); ?></h2>
    <ul class="hero__list">
      <?php while($activity->have_posts()): ?>
        <?php echo e($activity->the_post()); ?>

        <?php echo $__env->make('blocks.hero.hero-item', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php endwhile; ?>
    </ul>
    <a href="javascript://" class="hero__button button get-popup" data-popup="callback"><?php echo e(get_field('button', 'option')); ?></a>
  </div>
</section>
