<section class="hero">
  <img src="<?php echo get_field('background', get_option('page_on_front')); ?>" alt="Vos experts en revêtement de façades " class="hero__img">

  <div class="hero__inner">
    <h2 class="hero__title subtitle">Vos experts en revêtement de façades </h2>
    <ul class="hero__list">
      <?php while($activity->have_posts()): ?>
        <?php echo e($activity->the_post()); ?>

        <?php echo $__env->make('blocks.hero.hero-item', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <?php endwhile; ?>
    </ul>
    <a href="javascript://" class="hero__button button get-popup" data-popup="callback">Contacter</a>
  </div>
</section>
