<ul class="services">
  <?php while($activity->have_posts()): ?>
    <?php echo e($activity->the_post()); ?>

    <?php echo $__env->make('blocks.services.services-item', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <?php endwhile; ?>
</ul>
