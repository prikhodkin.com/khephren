<section class="about">
  <div class="about__body">
    <div class="about__inner">
      <h2 class="about__title title"><?php echo e(get_field('about-title', get_option('page_on_front'))); ?></h2>
      <div class="about__content">
        <?php echo get_field('about-content',get_option('page_on_front') ); ?>


      </div>

    </div>
    <img src="<?php echo e(get_field('about-image', get_option('page_on_front'))); ?>" alt="Nous en quelques mots" class="about__img">
  </div>
  <div class="about__sub-text">
    <?php echo get_field('about-sub-text',get_option('page_on_front') ); ?>

  </div>
  <a href="/qui-sommes-nous/" class="about__button button">En savoir plus </a>

</section>
