<header class="header">
  <a <?php if(!is_front_page()): ?> href="<?php echo e(home_url('/')); ?>" <?php endif; ?>  class="header__logo">
    <img src="<?php echo e(get_field('logo','option')); ?>" alt="logo">
  </a>
  <a href="<?php echo e(App::NormalizePhone(get_field('phone', 'option'))); ?>" class="header__phone"><?php echo e(get_field('phone', 'option')); ?></a>
  <nav class="header__menu menu">
    <button class="menu__open">Menu</button>
    <div class="menu__inner">
      <button class="menu__close">
        <span class="visually-hidden">close</span>
        <?php echo $__env->make('partials.icons.close', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </button>
      <?php echo App::mainMenu(); ?>

    </div>
  </nav>
</header>
