<table>
  <tr>
    <td style='padding: 10px; border: #e9e9e9 1px solid;'><b>Nom et prénom :</b></td>
    <td style='padding: 10px; border: #e9e9e9 1px solid;'><?php echo e(wp_strip_all_tags($query['name'])); ?></td>
  </tr>
  <tr>
    <td style='padding: 10px; border: #e9e9e9 1px solid;'><b>Tél :</b></td>
    <td style='padding: 10px; border: #e9e9e9 1px solid;'><?php echo e(wp_strip_all_tags($query['phone'])); ?></td>
  </tr>
  <tr>
    <td style='padding: 10px; border: #e9e9e9 1px solid;'><b>Email :</b></td>
    <td style='padding: 10px; border: #e9e9e9 1px solid;'><?php echo e(wp_strip_all_tags($query['email'])); ?></td>
  </tr>
  <tr>
    <td style='padding: 10px; border: #e9e9e9 1px solid;'><b>Type :</b></td>
    <td style='padding: 10px; border: #e9e9e9 1px solid;'><?php echo e(wp_strip_all_tags($query['type'])); ?></td>
  </tr>
</table>
