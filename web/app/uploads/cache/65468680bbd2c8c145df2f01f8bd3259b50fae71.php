<section class="page-top" style="background-image: url(<?php echo e(get_field('background')); ?>)">
  <h1 class="page-top__title"><?php echo e(the_title()); ?></h1>
</section>
