<section class="portfolio">
  <h2 class="portfolio__title subtitle"><?php echo e(get_field('portfolio-title', get_option('page_on_front'))); ?></h2>
  <div class="swiper-container portfolio__slider">
    <ul class="portfolio__list swiper-wrapper">
      <?php echo App::printRepeatorField('portfolio-list', 'blocks.portfolio.portfolio-item', get_option('page_on_front')); ?>

    </ul>
    <div class="portfolio__controls controls">
      <button class="controls__button controls__button--prev" aria-label="prev">
        <?php echo $__env->make('partials.icons.prev', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </button>
      <button class="controls__button controls__button--next" aria-label="next">
        <?php echo $__env->make('partials.icons.next', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </button>
    </div>
  </div>
</section>
