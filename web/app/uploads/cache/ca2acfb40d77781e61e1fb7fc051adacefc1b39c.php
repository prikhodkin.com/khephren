<section class="advantages">
  <h2 class="advantages__title subtitle">Nous en quelques chiffres</h2>
  <ul class="advantages__list">
    <?php echo App::printRepeatorField('advantages-list', 'blocks.advantages.advantages-item', 'option'); ?>

  </ul>
</section>
