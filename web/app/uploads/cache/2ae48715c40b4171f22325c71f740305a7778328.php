<li class="services__item" id="<?php echo str_replace(" ", "-", strtolower(get_the_title())); ?>">
  <a class="services__img">
    <img src="<?php echo e(get_field('activity-image')); ?>" alt="<?php echo e(the_title()); ?>">
    <div class="services__inner">
      <p class="services__name">
        <?php if(get_field('activity-title')): ?>
          <?php echo e(get_field('activity-title')); ?>

        <?php else: ?>
          <?php echo e(the_title()); ?>

        <?php endif; ?>
      </p>
    </div>
  </a>
  <div class="services__body">
    <div class="services__content">
      <?php echo get_field('services-text'); ?>

    </div>
    <a href="javascript://" data-popup="callback" class="services__button button get-popup">Obtenir un devis</a>
  </div>
</li>
