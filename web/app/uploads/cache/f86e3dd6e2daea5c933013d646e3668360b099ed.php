<?php $__env->startSection('content'); ?>
  <?php echo $__env->make('partials.page-top', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="layout__content">
    <p class="layout__desc"><?php echo e(get_field('layout-desc')); ?></p>
    <?php echo $__env->make('blocks.services.services', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <section class="advantages">
      <h2 class="advantages__title subtitle"><?php echo e(get_field('adv-title')); ?></h2>
      <ul class="advantages__list advantages__list--center">
        <?php echo App::printRepeatorField('adv-list', 'blocks.advantages.advantages-item-short'); ?>

      </ul>
    </section>
    <?php echo $__env->make('blocks.contact.contact', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>

  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>