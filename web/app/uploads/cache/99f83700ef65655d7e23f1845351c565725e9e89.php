<section class="activity">
  <h2 class="activity__title subtitle"><?php echo e(get_field('activity-title', get_option('page_on_front'))); ?></h2>
  <ul class="activity__list">
    <?php while($activity->have_posts()): ?>
      <?php echo e($activity->the_post()); ?>

      <?php echo $__env->make('blocks.activity.activity-item', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php endwhile; ?>
  </ul>
  <a href="/nos-activites" class="activity__button button">En savoir plus </a>
</section>
