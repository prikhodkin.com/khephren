<?php $__env->startSection('content'); ?>
  <?php echo $__env->make('partials.page-top', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="layout__content">
    <?php echo $__env->make('blocks.breadcrumbs.breadcrumbs', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="layout__wrapper">
      <?php echo the_content(); ?>

    </div>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>