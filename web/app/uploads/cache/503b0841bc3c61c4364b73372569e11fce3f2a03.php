<li class="team__item human">
  <div class="human__top">
    <div class="human__desc">
      <p class="human__name"><?php echo e($row['name']); ?></p>
      <?php echo $row['desc']; ?>

    </div>
    <img src="<?php echo e($row['photo']); ?>" alt="<?php echo e($row['name']); ?>" class="human__img">
  </div>
  <div class="human__body">
    <div class="human__left">
      <?php echo $row['content-left']; ?>

    </div>
    <div class="human__right">
      <?php echo $row['content-right']; ?>

    </div>
  </div>
</li>
