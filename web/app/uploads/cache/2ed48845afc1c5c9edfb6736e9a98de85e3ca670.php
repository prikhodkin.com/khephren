<footer class="footer">
  <div class="footer__top">
    <a <?php if(!is_front_page()): ?> href="<?php echo e(home_url('/')); ?>" <?php endif; ?>  class="footer__logo">
      <img src="<?php echo e(get_field('logo','option')); ?>" alt="logo">
    </a>
    <nav class="footer__menu menu">
      <?php echo App::mainMenu(); ?>

    </nav>
  </div>
  <div class="footer__bottom">
    <p class="footer__copyright"><?php echo e(get_field('copyright','option')); ?></p>
    <ul class="footer__list">
      <li class="footer__item">
        <a href="<?php echo e(get_privacy_policy_url()); ?>">Politique de confidentialité</a>
      </li>
      <li class="footer__item">
        <a href="/mentions-legales">Mentions légales</a>
      </li>
    </ul>
    <a href="https://inalion.com" class="footer__development">
      <?php echo $__env->make('partials.icons.inalion', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      <span>Création des sites-web</span>
    </a>
  </div>
</footer>
<div id="cookie"></div>
<?php echo $__env->make('blocks.popup.popup', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
