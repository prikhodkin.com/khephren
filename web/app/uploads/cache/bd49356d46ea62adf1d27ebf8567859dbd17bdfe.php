<?php $__env->startSection('content'); ?>
  <?php echo $__env->make('partials.page-top', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  <div class="layout__content">
    <div class="team">
      <div class="team__header">
        <p class="team__bold"><?php echo e(get_field('team-header')); ?></p>
        <div class="team__text"><?php echo get_field('team-text'); ?></div>
      </div>
      <ul class="team__list">
        <?php echo App::printRepeatorField('team-list', 'blocks.team.team-item'); ?>

      </ul>
      <ul class="team__advs">
        <?php echo App::printRepeatorField('team-advs', 'blocks.team.team-adv'); ?>

      </ul>
      <div class="team__bottom">
        <h2 class="team__title subtitle">Nos qualifications</h2>
        <div class="team__row">
          <div class="team__content">
            <?php echo get_field('team-content'); ?>

          </div>
          <img src="<?php echo e(get_field('team-image')); ?>" alt="Nos qualifications" class="team__img">
        </div>
      </div>
    </div>
    <?php echo $__env->make('blocks.contact.contact', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
  </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>