<div class="popup" data-popup="callback">
  <div class="popup__wrapper">
    <div class="popup__content">
      <button class="popup__close">
        <?php echo $__env->make('partials.icons.close', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </button>
      <p class="popup__title subtitle">Contacter</p>
      <form class="popup__form js-callback form">
        <input type="hidden" name="action" value="callback_action">
        <label class="field">
          <input type="text" name="name" placeholder="Nom et prénom" class="field__input input">
        </label>
        <label class="field">
          <input type="phone" name="phone" placeholder="Numéro de tel" required class="field__input field__input--phone input">
        </label>
        <label class="field">
          <input type="email" name="email" placeholder="E-mail" class="field__input input">
        </label>
        <div class="popup__row">
          <label class="checkbox">
            <input type="radio" name="type" checked value="Professionnel">
            <span class="checkbox__circle"></span>
            <p class="checkbox__label">Professionnel</p>
          </label>
          <label class="checkbox">
            <input type="radio" name="type" value="Particulier">
            <span class="checkbox__circle"></span>
            <p class="checkbox__label">Particulier</p>
          </label>
        </div>
        <button type="submit" class="popup__button button">Contacter</button>
      </form>
    </div>
  </div>
</div>
<div class="popup popup--thanks" data-popup="thanks">
  <div class="popup__wrapper">
    <div class="popup__content">
      <button class="popup__close">
        <?php echo $__env->make('partials.icons.close', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
      </button>
      <p class="popup__title subtitle">Votre demande est bien reçu !</p>
      <p class="popup__text">Nous allons prendre contact avec vous afin de valider les éléments.</p>
    </div>
  </div>
</div>
