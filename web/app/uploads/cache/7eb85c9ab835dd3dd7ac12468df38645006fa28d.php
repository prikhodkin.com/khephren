<?php $__env->startSection('content'); ?>
    <?php echo $__env->make('partials.page-top', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <div class="layout__content layout__content--contact">
        <?php echo $__env->make('blocks.contact.contact', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
        <?php echo $__env->make('blocks.contact.contact-bottom', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>