<section class="advantages">
  <h2 class="advantages__title subtitle"><?php echo e(get_field('advantages-title', get_option('page_on_front'))); ?></h2>
  <ul class="advantages__list">
    <?php echo App::printRepeatorField('advantages-list', 'blocks.advantages.advantages-item', 'option'); ?>

  </ul>
</section>
