<li class="activity__item">
  <a href="/nos-activites/#<?php echo str_replace(" ", "-", strtolower(get_the_title())); ?>">
  <img src="<?php echo e(get_field('activity-image')); ?>" alt="<?php echo e(the_title()); ?>" class="activity__img">
  <div class="activity__inner">
    <p class="activity__name">
      <?php if(get_field('activity-title')): ?>
        <?php echo e(get_field('activity-title')); ?>

      <?php else: ?>
      <?php echo e(the_title()); ?>

      <?php endif; ?>
    </p>
  </div>
  </a>
</li>
