<section class="clients">
  <h2 class="clients__title subtitle">Ils nous font confiance</h2>
  <ul class="clients__list">
    <?php echo App::printRepeatorField('clients-list', 'blocks.clients.clients-item', 'option'); ?>

  </ul>
</section>
