<?php

namespace App;
use App\Classes\PostTypes;
use PostTypes\PostType;
use PostTypes\Taxonomy;


/**
 * Common Labels for All CPT
 */

$common_labels = [
    'add_new' => 'Добавить элемент',
    'add_new_item' => 'Создание нового элемента',
    'edit_item' => 'Редактировать элемент',
    'new_item' => 'Новый элементы',
    'view_item' => 'Просмотреть элемент',
    'view_items' => 'Просмотреть элементы',
    'search_items' => 'Поиск элементов',
    'not_found' => 'Ничего не найдено',
    'not_found_in_trash' => 'В корзине ничего не найдено',
    'all_items' => 'Все элементы',
];

//(new Taxonomy('articles_type'))
//    ->labels([
//        'name' => 'Тип',
//        'singular_name' => 'Тип',
//        'menu_name' => 'Типы статей',
//        'add_new_item' => 'Добавить элемент',
//        'new_item_name' => 'Название нового типа',
//        'all_items' => 'Все типы'
//    ])
//    ->options([
//        'show_in_rest' => true,
//    ])
////    ->options(['show_in_menu' => false, 'show_in_nav_menus' => false])
//    ->register();


(new PostType(PostTypes::SERVICES))
    ->labels($common_labels)
    ->names([
        'name' => 'services',
        'slug' => 'services',
        'singular' => 'Services',
        'plural' => 'Services'

    ])
    ->taxonomy('services_type')
    ->register();

(new PostType(PostTypes::FORMS))
    ->labels($common_labels)
    ->names([
        'name' => 'forms',
        'slug' => 'forms',
        'singular' => 'Forms',
        'plural' => 'Forms',
    ])
    ->options([
        'exclude_from_search' => true,
        'publicly_queryable' => false,
//        'show_in_admin_bar' => false
    ])
    ->register();
