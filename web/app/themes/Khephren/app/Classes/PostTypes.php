<?php

namespace App\Classes;

final class PostTypes
{
    const FORMS = 'forms';
    const SERVICES = 'services';
}
