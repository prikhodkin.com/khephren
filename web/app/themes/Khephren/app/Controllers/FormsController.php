<?php

namespace App\Controllers;

use App\Classes\PostTypes;
use Sober\Controller\Controller;

class FormsController extends Controller
{
    const HEADERS = [
        'content-type: text/html'
    ];

    const EMAIL = 'contact@khephren-facades.fr';

    public function callback_action()
    {
        self::send('blocks.email.mail-template', $_POST, self::EMAIL, 'Nouvelle demande de la part de votre Site-web Khephren Façades', 'Nouvelle demande de la part de votre Site-web Khephren Façades');
    }

    private function send($template, $query, $email, $title, $post_title)
    {

        $message = \App\template($template, compact('query'));
        $post_data = array(
            'post_type' => PostTypes::FORMS,
            'post_content' => $message,
            'post_status' => 'publish',
        );
        $post_id = wp_insert_post($post_data);
        wp_update_post(['ID' => $post_id, 'post_title' => $post_title.'#' . $post_id]);

        if (!empty($post_id)) {
            wp_mail($email, $title, $message, self::HEADERS);
            wp_send_json(['success' => true]);
        } else {
            wp_send_json(['success' => false]);
        }
    }
}
