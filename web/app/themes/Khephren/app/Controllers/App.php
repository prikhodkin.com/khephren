<?php

namespace App\Controllers;

use App\Classes\CustomWalker;
use App\Classes\FooterWalker;
use App\Classes\InnerWalker;
use Sober\Controller\Controller;
use function App\template;

class App extends Controller
{
    public function siteName()
    {
        return get_bloginfo('name');
    }

    public static function title()
    {
        if (is_home()) {
            if ($home = get_option('page_for_posts', true)) {
                return get_the_title($home);
            }
            return __('Latest Posts', 'sage');
        }
        if (is_archive()) {
            return get_the_archive_title();
        }
        if (is_search()) {
            return sprintf(__('Search Results for %s', 'sage'), get_search_query());
        }
        if (is_404()) {
            return __('Not Found', 'sage');
        }
        return get_the_title();
    }

    public static function printRepeatorField($field_name, $template, $option = '')
    {
        $rows = get_field($field_name, $option);
        $result = '';

        foreach ($rows as $key => $row) {
            $result .= template($template, compact('key', 'row'));
        }

        return $result;
    }


    public static function mainMenu()
    {
        $args = array(
            'theme_location' => 'top_navigation',
            'container'      => false,
            'items_wrap' => ' <ul class="menu__list">%3$s</ul>',
            'walker' => new CustomWalker()
        );
        return wp_nav_menu($args);
    }

    public static function innerMenu()
    {
        $args = array(
            'theme_location' => 'inner_navigation',
            'container'      => false,
            'items_wrap' => ' <ul class="menu-inside__list">%3$s</ul>',
            'walker' => new InnerWalker()
        );
        return wp_nav_menu($args);
    }

    public static function footerMenu()
    {
        $args = array(
            'theme_location' => 'footer_navigation',
            'container'      => false,
            'items_wrap' => ' <ul class="footer__list">%3$s</ul>',
            'walker' => new FooterWalker()
        );
        return wp_nav_menu($args);
    }



    public static function NormalizePhone($number, $minLength = 10)
    {
        $minLength = intval($minLength);
        if ($minLength <= 0 || strlen($number) < $minLength) {
            return false;
        }

        if (strlen($number) >= 10 && substr($number, 0, 2) == '+8') {
            $number = '00' . substr($number, 1);
        }

        $number = preg_replace("/[^0-9\#\*,;]/i", "", $number);
        if (strlen($number) >= 10) {
            if (substr($number, 0, 2) == '80' || substr($number, 0, 2) == '81' || substr($number, 0, 2) == '82') {
            } else if (substr($number, 0, 2) == '00') {
                $number = substr($number, 2);
            } else if (substr($number, 0, 3) == '011') {
                $number = substr($number, 3);
            } else if (substr($number, 0, 1) == '8') {
                $number = '7' . substr($number, 1);
            } else if (substr($number, 0, 1) == '0') {
                $number = substr($number, 1);
            }
        }

        return $number;
    }

    public function activity() {

        $args = array(
            'post_type'      => 'services',
            'post_status'    => 'publish',
            'posts_per_page' => -1,
            'orderby' => 'menu_order',
            'order' => 'ASC'
        );

        return new \WP_Query($args);
    }
}
