{{--
  Template Name: Main Page
--}}

@extends('layouts.app')

@section('content')
  @include('blocks.hero.hero')
  @include('blocks.about.about')
  @include('blocks.activity.activity')
  @include('blocks.portfolio.portfolio')
  <div class="container">
    @include('blocks.advantages.advantages')
    @include('blocks.clients.clients')
    @include('blocks.contact.contact')
  </div>
@endsection
