{{--
  Template Name: Client Page
--}}

@extends('layouts.app')

@section('content')
  @include('partials.page-top')
  <div class="layout__content">
    @include('blocks.clients.clients')
    @include('blocks.contact.contact')
  </div>
@endsection
