{{--
  Template Name: Content Page
--}}

@extends('layouts.app')

@section('content')
  @include('partials.page-top')
  <div class="layout__content">
    @include('blocks.breadcrumbs.breadcrumbs')
    <div class="layout__wrapper">
      {!! the_content() !!}
    </div>
  </div>
@endsection
