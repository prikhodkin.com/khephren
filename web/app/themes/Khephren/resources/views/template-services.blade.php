{{--
  Template Name: Services Page
--}}

@extends('layouts.app')

@section('content')
  @include('partials.page-top')
  <div class="layout__content">
    <p class="layout__desc">{{get_field('layout-desc')}}</p>
    @include('blocks.services.services')
    <section class="advantages">
      <h2 class="advantages__title subtitle">{{get_field('adv-title')}}</h2>
      <ul class="advantages__list advantages__list--center">
        {!! App::printRepeatorField('adv-list', 'blocks.advantages.advantages-item-short') !!}
      </ul>
    </section>
    @include('blocks.contact.contact')

  </div>
@endsection
