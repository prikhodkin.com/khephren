@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  @include('partials.page-top')
  <div class="layout__content">
    @include('blocks.breadcrumbs.breadcrumbs')
    <div class="layout__wrapper">
      {!! the_content() !!}
    </div>
  </div>
  @endwhile
@endsection
