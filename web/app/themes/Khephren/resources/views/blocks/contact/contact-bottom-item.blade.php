<li class="contact-bottom__item">
  <img src="{{$row['image']}}" alt="" class="contact-bottom__image">
  <div class="contact-bottom__text">
    {!! $row['text'] !!}
  </div>
</li>
