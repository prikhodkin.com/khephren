<section class="portfolio">
  <h2 class="portfolio__title subtitle">{{get_field('portfolio-title', get_option('page_on_front'))}}</h2>
  <div class="swiper-container portfolio__slider">
    <ul class="portfolio__list swiper-wrapper">
      {!! App::printRepeatorField('portfolio-list', 'blocks.portfolio.portfolio-item', get_option('page_on_front')) !!}
    </ul>
    <div class="portfolio__controls controls">
      <button class="controls__button controls__button--prev" aria-label="prev">
        @include('partials.icons.prev')
      </button>
      <button class="controls__button controls__button--next" aria-label="next">
        @include('partials.icons.next')
      </button>
    </div>
  </div>
</section>
