<li class="portfolio__item swiper-slide">
  <div class="portfolio__info">
    <div class="portfolio__inner">
      <p class="portfolio__name">{{$row['name']}}</p>
      <div class="portfolio__content">
        {!! $row['content'] !!}
      </div>
    </div>
  </div>
  <div class="swiper-container gallery">
    <ul class="gallery__list swiper-wrapper">
      @foreach($row['gallery'] as $item)
      <li class="gallery__item swiper-slide">
        <img src="{{$item['image']}}">
      </li>
      @endforeach
    </ul>
    <div class="gallery__controls controls">
      <button class="controls__button controls__button--prev" aria-label="prev">
        @include('partials.icons.prev')
      </button>
      <div class="controls__counter">
        <span class="controls__current">1</span>/<span class="controls__length">6</span>
      </div>
      <button class="controls__button controls__button--next" aria-label="next">
        @include('partials.icons.next')
      </button>
    </div>
  </div>
</li>
