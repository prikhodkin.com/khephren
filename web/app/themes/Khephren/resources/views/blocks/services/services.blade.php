<ul class="services">
  @while($activity->have_posts())
    {{$activity->the_post()}}
    @include('blocks.services.services-item')
    {{wp_reset_query()}}
  @endwhile
</ul>
