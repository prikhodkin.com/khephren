<li class="services__item" id="{!! str_replace(" ", "-", strtolower(get_the_title())) !!}">
  <a class="services__img">
    <img src="{{get_field('activity-image')}}" alt="{{the_title()}}">
    <div class="services__inner">
      <p class="services__name">
        @if (get_field('activity-title'))
          {{get_field('activity-title')}}
        @else
          {{the_title()}}
        @endif
      </p>
    </div>
  </a>
  <div class="services__body">
    <div class="services__content">
      {!! get_field('services-text') !!}
    </div>
    <a href="javascript://" data-popup="callback" class="services__button button get-popup">Obtenir un devis</a>
  </div>
</li>
