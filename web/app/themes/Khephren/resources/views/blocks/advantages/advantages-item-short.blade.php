<li class="advantages__item">
  <img src="{{$row['icon']}}" alt="{{$row['text']}}" class="advantages__icon">
  <p class="advantages__info">{!!$row['text']  !!}</p>
</li>
