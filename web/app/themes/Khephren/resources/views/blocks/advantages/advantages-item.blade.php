<li class="advantages__item">
  <img src="{{$row['a-image']}}" alt="{{$row['a-text']}}" class="advantages__icon">
  <span class="advantages__accent">{{$row['a-accent']}}</span>
  <p class="advantages__info">{{$row['a-text']}}</p>
</li>
