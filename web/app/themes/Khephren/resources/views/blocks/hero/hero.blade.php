<section class="hero">
  <img src="{!! get_field('background', get_option('page_on_front')) !!}" alt="{{get_field('hero-title', get_option('page_on_front'))}} " class="hero__img">

  <div class="hero__inner">
    <h2 class="hero__title subtitle">{{get_field('hero-title', get_option('page_on_front'))}}</h2>
    <ul class="hero__list">
      @while($activity->have_posts())
        {{$activity->the_post()}}
        @include('blocks.hero.hero-item')
      @endwhile
    </ul>
    <a href="javascript://" class="hero__button button get-popup" data-popup="callback">{{get_field('button', 'option')}}</a>
  </div>
</section>
