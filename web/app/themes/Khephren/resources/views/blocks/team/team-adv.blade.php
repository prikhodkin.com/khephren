<li class="team__adv">
  <img src="{{$row['icon']}}" alt="" class="team__icon">
  <div class="team__inner">
    {!! $row['content'] !!}
  </div>
</li>
