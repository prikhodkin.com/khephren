<li class="team__adv-item">
  <img src="{{ $row['icon'] }}" alt="">
  <div class="team__adv-content">
    {!! $row['content'] !!}
  </div>
</li>
