<li class="team__item human">
  <div class="human__top">
    <div class="human__desc">
      <p class="human__name">{{$row['name']}}</p>
      {!! $row['desc'] !!}
    </div>
    <img src="{{$row['photo']}}" alt="{{$row['name']}}" class="human__img">
  </div>
  <div class="human__body">
    <div class="human__left">
      {!! $row['content-left'] !!}
    </div>
    <div class="human__right">
      {!! $row['content-right'] !!}
    </div>
  </div>
</li>
