<li class="activity__item">
  <a href="/nos-activites/#{!! str_replace(" ", "-", strtolower(get_the_title())) !!}">
  <img src="{{get_field('activity-image')}}" alt="{{the_title()}}" class="activity__img">
  <div class="activity__inner">
    <p class="activity__name">
      @if (get_field('activity-title'))
        {{get_field('activity-title')}}
      @else
      {{the_title()}}
      @endif
    </p>
  </div>
  </a>
</li>
