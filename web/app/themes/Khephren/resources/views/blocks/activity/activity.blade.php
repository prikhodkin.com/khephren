<section class="activity">
  <h2 class="activity__title subtitle">{{get_field('activity-title', get_option('page_on_front'))}}</h2>
  <ul class="activity__list">
    @while($activity->have_posts())
      {{$activity->the_post()}}
      @include('blocks.activity.activity-item')
    @endwhile
  </ul>
  <a href="/nos-activites" class="activity__button button">Savoir plus</a>
</section>
