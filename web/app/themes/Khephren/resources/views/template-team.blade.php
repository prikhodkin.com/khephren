{{--
  Template Name: Team Page
--}}

@extends('layouts.app')

@section('content')
  @include('partials.page-top')
  <div class="layout__content">
    <div class="team">
      <ul class="team__adv-list">
        {!! App::printRepeatorField('team-adv-list', 'blocks.team.team-adv-item') !!}
      </ul>
      <div class="team__header">
        <p class="team__bold">{{get_field('team-header')}}</p>
        <div class="team__text">{!! get_field('team-text') !!}</div>
      </div>
      <ul class="team__list">
        {!! App::printRepeatorField('team-list', 'blocks.team.team-item') !!}
      </ul>
      <ul class="team__advs">
        {!! App::printRepeatorField('team-advs', 'blocks.team.team-adv') !!}
      </ul>
      <div class="team__bottom">
        <h2 class="team__title subtitle">Nos qualifications</h2>
        <div class="team__row">
          <div class="team__content">
            {!! get_field('team-content') !!}
          </div>
          <img src="{{get_field('team-image')}}" alt="Nos qualifications" class="team__img">
        </div>
      </div>
    </div>
    @include('blocks.contact.contact')
  </div>
@endsection
