{{--
  Template Name: Contact Page
--}}

@extends('layouts.app')

@section('content')
    @include('partials.page-top')
    <div class="layout__content layout__content--contact">
        @include('blocks.contact.contact')
        @include('blocks.contact.contact-bottom')
    </div>
@endsection
