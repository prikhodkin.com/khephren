<footer class="footer">
  <div class="footer__top">
    <a @if(!is_front_page()) href="{{ home_url('/') }}" @endif  class="footer__logo">
      <img src="{{get_field('logo','option')}}" alt="logo">
    </a>
    <nav class="footer__menu menu">
      {!! App::mainMenu() !!}
    </nav>
  </div>
  <div class="footer__bottom">
    <p class="footer__copyright">{{get_field('copyright','option')}}</p>
    <ul class="footer__list">
      <li class="footer__item">
        <a href="{{get_privacy_policy_url()}}">Politique de confidentialité</a>
      </li>
      <li class="footer__item">
        <a href="/mentions-legales">Mentions légales</a>
      </li>
    </ul>
    <a href="https://inalion.com" class="footer__development">
      @include('partials.icons.inalion')
      <span>Création des sites-web</span>
    </a>
  </div>
</footer>
<div id="cookie"></div>
@include('blocks.popup.popup')
