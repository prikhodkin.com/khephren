<header class="header">
  <a @if(!is_front_page()) href="{{ home_url('/') }}" @endif  class="header__logo">
    <img src="{{get_field('logo','option')}}" alt="logo">
  </a>
  <a href="{{App::NormalizePhone(get_field('phone', 'option'))}}" class="header__phone">{{get_field('phone', 'option')}}</a>
  <nav class="header__menu menu">
    <button class="menu__open">Menu</button>
    <div class="menu__inner">
      <button class="menu__close">
        <span class="visually-hidden">close</span>
        @include('partials.icons.close')
      </button>
      {!! App::mainMenu() !!}
    </div>
  </nav>
</header>
