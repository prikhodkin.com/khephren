<section class="page-top" style="background-image: url({{get_field('background')}})">
  <h1 class="page-top__title">{{the_title()}}</h1>
</section>
