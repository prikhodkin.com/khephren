/* eslint-disable */
import React, {useState} from "react";
import CustomScroll from 'react-custom-scroll';
import CookieLink from "./CookieLink";
import Toggle from "./Toggle";
import CookieItem from "./CookieItem";

const DetailCookie = (props) => {

  return (
    <CustomScroll allowOuterScroll={true}>
      <div className="cookie__scroll">
        <p className="cookie__caption">Vos préférences cookies</p>

        <p className="cookie__text">Choisissez quels cookies vous souhaitez activer ou désactiver.</p>
        <CookieLink linkHandler={props.linkHandler}/>
        <div className="cookie__row">
          <p className="cookie__all">Tout cocher</p>
          <Toggle
            checkedStatus={props.checkedStatus}
            toggleHandler={props.checkAll}
          />
        </div>
        <ul className="cookie__list">
          <CookieItem
            isChecked={true}
            isDisabled={true}
            checkedStatus={props.checkedStatus}
            label="Indispensables"
            txt="Certains cookies sont indispensables au fonctionnement du site et ne peuvent être refusés. Vous pouvez paramétrer les autres types de cookies à tout moment dans l’onglet  « paramétrer »."
          />
          <CookieItem
            label="Statistiques et audience"
            txt="Ces cookies nous permettent de mieux mesurer les performances de notre site et de nos fonctionnalités en ayant une idée claire des volumes de fréquentation et d’utilisation. CYCOVER ne partagera en aucun cas ces données avec des tiers."
            checkedStatus={props.checkedStatus}
          />
          <CookieItem
            label="Expérience et relation"
            txt="Nous utilisons ces cookies pour détecter de potentiels problèmes de conception sur notre site, mais aussi dialoguer avec vous et construire une relation de confiance."
            checkedStatus={props.checkedStatus}
          />
          <CookieItem
            label="Annonces personnalisées"
            txt="Ces cookies marketing nous permettent de définir si vous êtes arrivés sur notre site via une annonce, mais aussi de vous proposer des annonces plus qualitatives (moins redondantes et plus conformes à vos attentes)."
            checkedStatus={props.checkedStatus}
          />
        </ul>
        <div className="cookie__both"></div>
      </div>
    </CustomScroll>
  )
}

export default DetailCookie
