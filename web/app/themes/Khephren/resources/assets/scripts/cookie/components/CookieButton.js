/* eslint-disable */
import React from "react";
import TouchIcon from "./icons/TouchIcon";

const CookieButton = (props) => {
  return (
    <button onClick={props.showCookie} className="cookie__button">
      <TouchIcon />
    </button>
  )
}

export default CookieButton;
