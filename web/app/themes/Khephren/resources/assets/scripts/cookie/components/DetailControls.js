/* eslint-disable */
import React from "react";
import Control from "./Control";

const DetailControls = (props) => {
  return (
    <div>
      <Control controlHandler={props.showDetail} name="Retour"/>
      <Control controlHandler={props.showDetail} name="J'accepte tout"/>
      <Control controlHandler={props.controlHandler} name="Terminer"/>
    </div>
  )
}

export default DetailControls;
