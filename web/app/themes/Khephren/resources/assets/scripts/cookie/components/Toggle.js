/* eslint-disable */
import React from "react";

const Toggle = (props) => {

  return (
    <div className={(!props.isSmall) ? "cookie__toggle toggle" : "cookie__toggle toggle toggle--small"}>
      <label>
        <input  checked={props.isChecked || props.check || props.checkedStatus }
                disabled={props.isDisabled}
                onChange={props.toggleHandler}
                type="checkbox"
                className="toggle__input"
        />
        <span className="toggle__box"></span>
      </label>
    </div>
  )
}

export default Toggle;
