/* eslint-disable */
import React from "react";
import CookieLink from "./CookieLink";

const WelcomeCookie = (props) => {
  return (
    <div className="cookie__content">
      <p className="cookie__sub-title">Gestion de vos préférences sur</p>
      <p className="cookie__title">les Cookies !</p>
      <p className="cookie__text">Pour optimiser votre expérience, nous utilisons des cookies sur notre site internet.</p>
      <CookieLink linkHandler={props.linkHandler}/>
    </div>
  )
}

export default WelcomeCookie;
