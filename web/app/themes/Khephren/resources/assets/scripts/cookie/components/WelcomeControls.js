/* eslint-disable */
import React from "react";
import Control from "./Control";

const WelcomeControls = (props) => {
  return (
    <div>
      <Control controlHandler={props.controlHandler} name="Tout refuser"/>
      <Control controlHandler={props.showDetail} name="Paramétrer"/>
      <Control controlHandler={props.controlHandler} name="Accepter tout"/>
    </div>
  )
}

export default WelcomeControls
