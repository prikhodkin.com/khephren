/* eslint-disable */
import React, {useState} from "react";
import Toggle from "./Toggle";

const CookieItem = (props) => {
  const [isChecked, setIsChecked] = useState(false);
  const changeChecked = () => {
    setIsChecked(!isChecked)
  }

  return (
    <li className="cookie__item">
      <Toggle
        isChecked={props.isChecked}
        isDisabled={props.isDisabled}
        checkedStatus={props.checkedStatus}
        isSmall={true}
        toggleHandler={changeChecked}
        check={isChecked}
      />
      <div className="cookie__right">
        <p className="cookie__label">{props.label}</p>
        <p className="cookie__txt">{props.txt}</p>
      </div>
    </li>
  )
}

export default CookieItem;
