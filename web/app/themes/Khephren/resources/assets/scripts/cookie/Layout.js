/* eslint-disable */
import React, {useState} from "react";
import CookieButton from "./components/CookieButton";
import CookieWrapper from "./components/CookieWrapper";
import { useCookies } from "react-cookie";

import { CSSTransition, TransitionGroup } from 'react-transition-group'

const Layout = () => {
  const [isShown, setIsShown] = useState(false);
  const [isDetail, setIsDetail] = useState(true);
  const [isChecked, setIsChecked] = useState(false);
  const [cookies, setCookie, removeCookie] = useCookies(["cookie"]);

  console.log(cookies)



  const changeChecked = () => {
    setIsChecked(!isChecked)
  }


  const showCookie = () => {
    setIsShown(!isShown)
    setCookie("cookie", true)
  }

  const showDetail = () => {
    setIsDetail(!isDetail);
  }



  const politic = document.querySelector(`.politic--first`);
  const overlay = document.querySelector(`.overlay`);
  const showPolitic = () => {
    const scrollY = window.pageYOffset;
    politic.style.top = scrollY + document.documentElement.clientHeight / 2 - 250 + "px"
    politic.classList.add(`politic--active`);
    overlay.classList.add(`overlay--active`);
  }


  return (
    <div className="cookie">
      <CookieButton showCookie={showCookie} />
        <TransitionGroup>
          {(isShown || !cookies.cookie ) && (
            <CSSTransition timeout={300} classNames="slide">
              <CookieWrapper
                showDetail={showDetail}
                isDetail={isDetail}
                controlHandler={showCookie}
                checkedStatus={isChecked}
                checkAll={changeChecked}
                linkHandler={showPolitic}
              />

            </CSSTransition>
          )}
        </TransitionGroup>
    </div>
  )
}
export default Layout;
