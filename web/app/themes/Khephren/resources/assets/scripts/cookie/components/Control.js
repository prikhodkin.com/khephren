/* eslint-disable */
import React from "react";

const Control = (props) => {
  return (
    <button onClick={props.controlHandler} className="cookie__control">{props.name}</button>
  )
}

export default Control;
