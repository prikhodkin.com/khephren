/* eslint-disable */
// import external dependencies
import 'jquery';

// Import everything from autoload
import './autoload/**/*'
import {Popup, PopupThanks} from './util/popup';
import IMask from 'imask';
import React from 'react';
import ReactDOM from 'react-dom';
import { CookiesProvider } from 'react-cookie';

import Layout from './cookie/Layout';

const domContainer = document.querySelector('#cookie');
ReactDOM.render(<CookiesProvider>
  <Layout/>
</CookiesProvider>, domContainer);


// import local dependencies
import Router from './util/Router';
import common from './routes/common';
import home from './routes/home';
import aboutUs from './routes/about';

/** Populate Router instance with DOM routes */
const routes = new Router({
  // All pages
    common,
  // Home page
    home,
  // About Us page, note the change from about-us to aboutUs.
    aboutUs,
});

// Load Events
jQuery(document).ready(() => routes.loadEvents());

$('.menu__open').on('click', function () {
    $('.menu__inner').addClass('menu__inner--active');
});

$('.menu__close').on('click', function () {
    $('.menu__inner').removeClass('menu__inner--active');
});


const popups = document.querySelectorAll('.popup');
const popupThank = document.querySelector('.popup--thanks');

popups.forEach(function (popup) {
    new Popup(popup);
});

const ajaxSend = (url, method, data) => {
    return fetch(url, {
        method: method,
        headers: {
            'X-Requested-With': 'XMLHttpRequest',
        },
        body: data,
    })
    .then(
        function (response) {
            if (response.status !== 200) {
                console.log('Looks like there was a problem. Status Code: ' +
                response.status);
                return;
            }
            console.log('success');
            return response.json();
        }
    )
    .catch(error => console.log(error))
}


const send = (evt) => {
    evt.preventDefault();
    const {target} = evt;
    const formData = new FormData(target);
    ajaxSend(window.global.ajax_url, 'post', formData).then((data) => console.log(data));
    new PopupThanks(popupThank).openPopup();
    target.reset();
}

const forms = document.querySelectorAll('form');

forms.forEach(function (item) {
    item.addEventListener('submit', function (evt) {
        send(evt);
    })
})


const phones = document.querySelectorAll('.field__input--phone');
const phoneOption = {
    mask: '{+33} 0 00 00 00 00',
}

// Инициализация маски для телефона
phones.forEach((item) => {
    IMask(item, phoneOption);
});
