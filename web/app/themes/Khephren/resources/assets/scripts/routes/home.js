import Swiper from 'swiper/bundle';


export default {
    init() {
        const porftfolio = document.querySelector('.portfolio__slider');
        const porftfolioControls = document.querySelector('.portfolio__controls');
        const porftfolioPrev = porftfolioControls.querySelector('.controls__button--prev');
        const porftfolioNext = porftfolioControls.querySelector('.controls__button--next');
        const porftolioItems = document.querySelectorAll('.portfolio__item');
        setTimeout(()=> {
            new Swiper(porftfolio, {
                navigation: {
                    nextEl: porftfolioNext,
                    prevEl: porftfolioPrev,
                },
                breakpoints: {
                    1280: {
                        slidesPerView: 'auto',
                        spaceBetween: 157,
                        centeredSlides: true,
                    },
                },
            });
        },300)


        porftolioItems.forEach((item) => {
            const gallery = item.querySelector('.gallery');
            const prev = item.querySelector('.controls__button--prev');
            const next = item.querySelector('.controls__button--next');
            const length = gallery.querySelectorAll('.gallery__item');
            const controlLength = item.querySelector('.controls__length');
            const current = item.querySelector('.controls__current');

            controlLength.innerHTML = length.length;
            setTimeout(() => {
                const slider = new Swiper(gallery, {
                    slidesPerView: 1,
                    allowTouchMove: false,
                    navigation: {
                        nextEl: next,
                        prevEl: prev,
                    },


                });

              slider.on('slideChange', function () {
                currentSlide()
              });
            function currentSlide()
            {
                console.log(slider.activeIndex + 1);
                current.innerHTML = slider.activeIndex + 1;
            }
            }, 300)
        })




    },
    finalize() {

    },
};
